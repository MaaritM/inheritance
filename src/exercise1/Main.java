package exercise1;

public class Main {
    public static void main(String[] args) {

       Circle circle1 = new Circle(50, 25);
       Rectangle rectangle1 = new Rectangle(25, 20);

       rectangle1.setArea(50);

        System.out.println(rectangle1.getArea());

        circle1.setPerimeter(40);

        System.out.println("The perimeter of the circle1 is " + circle1.getPerimeter() + " and the area of circle1 is " + circle1.getArea() );


    }
}
