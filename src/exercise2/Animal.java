package exercise2;

public class Animal {
    private String gender;
    private String name;
    private int age;
    private String voice;

    public Animal(String gender, String name, int age, String voice) {
        this.gender = gender;
        this.name = name;
        this.age = age;
        this.voice = voice;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String yieldVoice() {
        return voice;
    }
}
