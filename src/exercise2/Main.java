package exercise2;

public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("boy", "kitty", 25, "meow");
        Dog dog1 = new Dog("girl", "doggy", 1, "woof-woof");

        Animal[] animals = {cat1, dog1};
        for(Animal animalVoice : animals) {
            System.out.println(animalVoice.yieldVoice());
        }
    }
}
